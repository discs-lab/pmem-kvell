# Readme


As a by-product of expediency the code in this project contains some hard-coded absolute file paths, at the following locations:
- Inside of each binary's `/NotConcurrent_Thread*/include/nvm_mgr.h` filename variable, designating the file to `mmap()` for that particular thread's binary.
- Inside the `/Benchmarks/YCSB_*/**Tree/*.benchmark` files, designating where to find the compiled binaries corresponding to that particular benchmark and the proper settings to launch it.
- Inside the `/Benchmarks/*Tree-YCSB_*.sh` files, launching the *.benchmark files, to create the files to `mmap()` at the hard-coded paths.


It is therefore simpler to execute the suite of instructions having the following directory as set as the working directory:
`/optane-data/hperez6`


## Step 1 - Clone the repository, if necessary
`git clone https://gitlab.cs.mcgill.ca/discs-lab/pmem-kvell`


## Step 2 - Build the suite of binaries
`./build.me`


## Step 3 - Run the benchmark suite
`cd Benchmarks`


`./run.me`

All the executable files should have the right `+x` permisions already set. If not, `build.me`, `/Benchmarks/run.me`, and all the `.sh` in `/Benchmarks` will need to have their permissions changed accordingly.
